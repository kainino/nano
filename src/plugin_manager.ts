import * as Discord from 'discord.js';
import storage from 'node-persist';

import { PseudoID, FakeMessage, pseudoIDToFakeUser, RealOrFakeUser } from './fakeuser';

export type OnLoad = ((instance: Instance) => void) | ((instance: Instance) => Promise<void>);

export interface VerbDescriptor {
  handler: (msg: FakeMessage, args: string) => Promise<void>;
  desc: string;
}
export type VerbMap = Map<string, VerbDescriptor>;

export type MessageListener = ((msg: FakeMessage) => void) | ((msg: FakeMessage) => Promise<void>);

export type ReactionListener =
  | ((reaction: Discord.MessageReaction, pseudoID: PseudoID) => void)
  | ((reaction: Discord.MessageReaction, pseudoID: PseudoID) => Promise<void>);

export interface Instance {
  readonly client: Discord.Client;
  readonly diagnosticChannel: Discord.TextChannel;
  pseudoIDToFakeUser(pseudoID: PseudoID): Promise<RealOrFakeUser>;
  storageSetItem(k: string, v: unknown): void;
  storageGetItem(k: string): unknown;
  storageRemoveItem(k: string): void;
}

interface Plugin {
  readonly onload?: OnLoad;
  readonly verbs?: VerbMap;
  readonly listener?: MessageListener;
  readonly reactionListener?: ReactionListener;
}

export class PluginManager {
  readonly onloads: (readonly [string, OnLoad])[] = [];
  readonly verbs: VerbMap = new Map();
  readonly listeners: MessageListener[] = [];
  readonly reactionListeners: ReactionListener[] = [];
  initialized = false;

  constructor() {
    this.verbs.set('help', {
      handler: async msg => {
        const helpmsgs = [];
        for (const [verb, { desc }] of this.verbs) {
          helpmsgs.push(`/${verb} ${desc}`);
        }
        await msg.reply(helpmsgs.join('\n'));
      },
      desc: '- print this help message',
    });
  }

  registerPlugin(name: string, plugin: Plugin) {
    if (plugin.onload) {
      this.onloads.push([name, plugin.onload]);
    }

    if (plugin.verbs) {
      plugin.verbs.forEach((v, k) => {
        this.verbs.set(k, v);
      });
    }

    if (plugin.listener) {
      this.listeners.push(plugin.listener);
    }

    if (plugin.reactionListener) {
      this.reactionListeners.push(plugin.reactionListener);
    }
  }

  /**
   * Initialize persistent storage and call plugin onload()s.
   */
  async onload(client: Discord.Client, diagnosticChannel: Discord.TextChannel) {
    await storage.init({
      dir: '_localdata/persistent',
    });

    for (const [name, onload] of this.onloads) {
      const instance: Instance = {
        client,
        diagnosticChannel,
        pseudoIDToFakeUser: pseudoID => pseudoIDToFakeUser(client, pseudoID),
        storageSetItem: (k, v) => storage.setItem(name + '/' + k, v),
        storageGetItem: k => storage.getItem(name + '/' + k),
        storageRemoveItem: k => storage.removeItem(name + '/' + k),
      };
      await onload(instance);
    }
  }

  /**
   * Call plugin verbs and listeners.
   *
   * Note: futurepunch assumes verbs happen before listeners.
   */
  async onmessage(fakemsg: FakeMessage): Promise<void> {
    if (fakemsg.content[0] === '/' || fakemsg.content[0] === '\\') {
      const verb = fakemsg.content.split(' ', 1)[0].slice(1);
      const args = fakemsg.content.slice(2 + verb.length);

      for (const [v, { handler }] of this.verbs) {
        if (v === verb) {
          try {
            await handler(fakemsg, args);
          } catch (e) {
            fakemsg.reply(e.stack ?? String(e));
          }
        }
      }
    }

    for (const listener of this.listeners) {
      await listener(fakemsg);
    }
  }

  /**
   * Call plugin reaction listeners.
   *
   * Note: Only catches reactions to locally-cached messages, but hopefully good enough.
   * */
  async onreaction(reaction: Discord.MessageReaction, userid: string) {
    for (const reactionListener of this.reactionListeners) {
      await reactionListener(reaction, userid);
    }
  }
}
