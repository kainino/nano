import assert from 'assert';

import { Snowflake } from 'discord.js';
import serialize from 'serialize-javascript';

import { FakeMessage } from '../fakeuser';
import { VerbDescriptor, Instance } from '../plugin_manager';

let instance: Instance;
let autorepliesPerServer: Map<
  Snowflake,
  Map<
    string,
    {
      trigger: RegExp;
      response: string;
    }
  >
>;

export async function onload(_instance: Instance) {
  instance = _instance;

  const state = await instance.storageGetItem('autoreplies');
  if (state) {
    autorepliesPerServer = eval('(' + state + ')');
  } else {
    autorepliesPerServer = new Map();
  }
}

export const verbs = new Map<string, VerbDescriptor>();

const kUsageMessage = `\
    /autoreply list - List autoreplies for this server
    /autoreply set ["regexp", "response"] - Add an autoreply
    /autoreply delete "regexp" - Remove an autoreply`;

verbs.set('autoreply', {
  handler: async (msg, args) => {
    if (msg.guild === null) return;
    let autoreplies = autorepliesPerServer.get(msg.guild.id);
    if (autoreplies === undefined) {
      autoreplies = new Map();
      autorepliesPerServer.set(msg.guild.id, autoreplies);
    }

    const space = args.indexOf(' ');
    const op = space === -1 ? args : args.slice(0, space);
    const rest = args.slice(space + 1);
    if (op === 'list') {
      const list = Array.from(autoreplies.entries())
        .map(([trigger, { response }]) => JSON.stringify([trigger, response]))
        .join('\n');
      await msg.reply('autoreplies for this server:\n' + list);
    } else if (op === 'set') {
      const json = JSON.parse(rest) as [string, string];
      assert(json instanceof Array);
      assert(json.length === 2);

      const autoreply = {
        trigger: new RegExp(json[0], 'i'),
        response: json[1],
      };
      autoreplies.set(json[0], autoreply);

      instance.storageSetItem('autoreplies', serialize(autorepliesPerServer));
      await msg.reply(`set ${autoreply.trigger} => ${JSON.stringify(autoreply.response)}`);
    } else if (op === 'delete') {
      const json = JSON.parse(rest) as string;
      assert(typeof json === 'string');

      const old = autoreplies.get(json);
      autoreplies.delete(json);

      instance.storageSetItem('autoreplies', serialize(autorepliesPerServer));
      if (old !== undefined) {
        await msg.reply(`deleted ${old.trigger}`);
      } else {
        await msg.reply(`no autoreply for ${JSON.stringify(json)}`);
      }
    } else {
      await msg.reply('Usage:\n' + kUsageMessage);
    }
  },
  desc: '\n' + kUsageMessage,
});

export async function listener(msg: FakeMessage) {
  if (msg.guild === null) return;
  const autoreplies = autorepliesPerServer.get(msg.guild.id);
  if (autoreplies === undefined) return;
  for (const [, { trigger, response }] of autoreplies) {
    if (trigger.test(msg.content)) {
      msg.channel.send(response);
    }
  }
}
