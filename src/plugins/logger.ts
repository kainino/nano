import * as assert from 'assert';
import * as crypto from 'crypto';
import * as fs from 'fs';
import { IncomingMessage } from 'http';
import * as https from 'https';
import * as path from 'path';

import { SnowflakeUtil, TextChannel, DMChannel, Message, Snowflake, Guild } from 'discord.js';
import * as cron from 'node-cron';

import { Instance, VerbDescriptor } from '../plugin_manager';

interface DiscordAttachment {
  readonly url: string;
  readonly id: Snowflake;
  readonly name: string | null;
}

interface SavedAttachment {
  originalUrl: string;
  sha256: string;
  localFilename: string;
}

interface SavedReactionMap {
  [emoji: string]: { users: Snowflake[] };
}

interface LoggedMessage {
  timestamp: string;
  author: { username: string; id: Snowflake };
  msg: string;
  id: Snowflake;
  attachments?: SavedAttachment[];
  attachmentsInText?: SavedAttachment[];
  reactions?: SavedReactionMap;
}

interface GuildMeta {
  members: { [id: string]: { username: string } };
  roles: { [id: string]: { name: string } };
  textChannels: { [id: string]: { name: string } };
}

export const verbs = new Map<string, VerbDescriptor>();

let instance: Instance;
export function onload(_instance: Instance) {
  instance = _instance;

  // Every day at 11am UTC:
  cron.schedule(
    '0 11 * * *',
    async () => {
      // Get yesterday's date (at 00:00Z).
      const yesterday = new Date();
      yesterday.setDate(yesterday.getDate() - 1);
      const yesterdayString = yesterday.toISOString().substring(0, 10);

      // Capture logs for that week.
      // (This will recapture a week 7 times, the last of which will be complete.)
      await logall(yesterdayString, 1);
      await instance.diagnosticChannel.send('Done!');
    },
    { timezone: 'Etc/UTC' }
  );
}

async function* messagesFromStartToNow(
  channel: TextChannel | DMChannel,
  start: Date
): AsyncGenerator<[Snowflake, Message]> {
  const limit = 100;
  const startID = SnowflakeUtil.generate(start);
  let after = startID;
  while (true) {
    const messagesReversed = await channel.messages.fetch({ limit, after });
    const messages = new Map([...messagesReversed.entries()].sort());
    if (messages.size === 0) {
      return;
    }
    for (const [id, m] of messages) {
      // Time should be ascending.
      if (id <= after) {
        throw 'messages not in chronological order';
      }
      after = id;

      // Fetch full message info, which includes reactions.
      await m.fetch();
      yield [id, m];
    }
  }
}

async function* messagesFromStartToEnd(
  channel: TextChannel | DMChannel,
  start: Date,
  end: Date
): AsyncGenerator<[Snowflake, Message]> {
  const endID = SnowflakeUtil.generate(end);
  if (channel.id >= endID) {
    // Channel had not been created yet.
    return;
  }
  const before = endID;
  const messages = messagesFromStartToNow(channel, start);
  for await (const [id, m] of messages) {
    if (id >= before) {
      break;
    }
    yield [id, m];
  }
}

function date10amUTCToFilename(date: Date): string {
  let str = date.toISOString(); // 2018-09-04T06:22:35.077Z
  str = str.substring(0, 10); // 2018-09-04
  str = str + ' 10Z'; // 2018-09-04 10Z
  return str;
}

function roundToPreviousMondayMorning(date: string): Date {
  const d = new Date(date);
  const dowRelativeToMonday = (d.getUTCDay() + 6) % 7;
  d.setDate(d.getDate() - dowRelativeToMonday);
  // Split days at 2am PST = 3am PDT = 5am EST = 6am EDT = 10am UTC.
  d.setTime(d.getTime() + 10 * 60 * 60 * 1000);
  return d;
}
assert.equal(
  roundToPreviousMondayMorning('2018-11-04').toUTCString().substring(0, 16),
  'Mon, 29 Oct 2018'
);
assert.equal(
  roundToPreviousMondayMorning('2018-11-05').toUTCString().substring(0, 16),
  'Mon, 05 Nov 2018'
);

function getDateRangeList(start: string, numWeeks: number): [Date, Date][] {
  const startRounded = roundToPreviousMondayMorning(start);

  const ranges: [Date, Date][] = [];
  for (let i = 0; i < numWeeks; ++i) {
    const dayStart = new Date(startRounded.getTime());
    dayStart.setDate(dayStart.getDate() + i * 7);

    const dayEnd = new Date(dayStart);
    dayEnd.setDate(dayEnd.getDate() + 7);

    ranges.push([dayStart, dayEnd]);
  }
  return ranges;
}

async function mkdirIfNeeded(path: string) {
  try {
    await fs.promises.mkdir(path);
  } catch (e) {
    if (e.code !== 'EEXIST') {
      throw e;
    }
  }
}

const attachmentUrlRegex = new RegExp(
  '\\b(https://cdn.discordapp.com/attachments/\\d+/(\\d+)/(\\S+))(\\s|$)',
  'gm'
);
function matchAttachmentUrls(msg: string): DiscordAttachment[] {
  const matches = msg.matchAll(attachmentUrlRegex);
  return Array.from(matches, ([, url, id, name]) => ({ url, name, id }));
}

const logsdir = path.join('_localdata', 'chatlogs');
const attachmentContentDir = path.join(logsdir, 'attachment_content');

async function impl(
  channel: TextChannel | DMChannel,
  start: string,
  numWeeks: number
): Promise<[Message | undefined, LogFileInfo[]]> {
  const dateRanges = getDateRangeList(start, numWeeks);
  const startDate = dateRanges[0][0];
  const endDate = dateRanges[dateRanges.length - 1][1];

  await mkdirIfNeeded(logsdir);
  let filePathBase;
  let channelName;
  if (channel.type === 'dm') {
    filePathBase = path.join(logsdir, `DM user=${channel.recipient.id} ch=${channel.id}`);
    channelName = `@${channel.recipient.tag}`;
    await mkdirIfNeeded(filePathBase);
  } else if (channel.type === 'text') {
    const serverPathBase = path.join(logsdir, `server=${channel.guild.id}`);
    await mkdirIfNeeded(serverPathBase);
    filePathBase = path.join(serverPathBase, `ch=${channel.id}`);
    await mkdirIfNeeded(filePathBase);
    channelName = `#${channel.name}`;
  } else {
    throw 'unknown channel type';
  }

  let statusMsg: Message | undefined;

  // Capture logs and write to files.
  const filenames = [];
  for (const [dayStart, dayEnd] of dateRanges) {
    const dayStartName = date10amUTCToFilename(dayStart);
    const dayEndName = date10amUTCToFilename(dayEnd);
    const filename = `${dayStartName}--${dayEndName}.json`;

    const messages = messagesFromStartToEnd(channel, dayStart, dayEnd);
    const messagesOut: string[] = [];
    for await (const [id, m] of messages) {
      if (!statusMsg) {
        // Happens just once, but avoids sending a message if not relevant.
        statusMsg = await instance.diagnosticChannel.send(
          `**${date10amUTCToFilename(startDate)}** ... **${date10amUTCToFilename(endDate)}** ` +
            `(next = \`${endDate.toISOString().substring(0, 10)}\`) ${channel}`
        );
      }

      const mOut: LoggedMessage = {
        timestamp: SnowflakeUtil.deconstruct(id).date.toISOString(),
        author: { username: m.author.username, id: m.author.id },
        msg: m.content,
        id,
      };

      for (const attachment of m.attachments.values()) {
        const savedAttachment = await saveAttachment(filePathBase, attachment);
        mOut.attachments ??= [];
        mOut.attachments.push(savedAttachment);
      }
      for (const attachment of matchAttachmentUrls(m.content)) {
        const savedAttachment = await saveAttachment(filePathBase, attachment);
        mOut.attachmentsInText ??= [];
        mOut.attachmentsInText.push(savedAttachment);
      }

      for (const [, reaction] of m.reactions.cache) {
        const users = await reaction.users.fetch();
        mOut.reactions ??= {};
        mOut.reactions[reaction.emoji.toString()] = { users: [...users.keys()] };
      }

      messagesOut.push(JSON.stringify(mOut, undefined, 1));
    }
    if (messagesOut.length) {
      filenames.push({ filename, alreadyExisted: fs.existsSync(filename) });
      const messagesJSON = '[\n' + messagesOut.join(',\n') + '\n]';
      /* noawait */ writeTextFileSanitized(filePathBase, filename, messagesJSON);
    }
  }

  // Keep a record of the known past channel names (human-browseable).
  {
    const data = JSON.stringify({ thisNameLastSeenOn: new Date() }, undefined, 1);
    /* noawait */ writeTextFileSanitized(filePathBase, `!aka ${channelName}.json`, data);
  }

  return [statusMsg, filenames];
}

// eslint-disable-next-line no-control-regex
const badCharacters = /[/?<>\\:*|":\x00-\x1f\x80-\x9f]/g;
function sanitizeFilename(filename: string): string {
  return filename.replace(badCharacters, '\ufffd');
}
async function writeTextFileSanitized(
  filePathBase: string,
  filename: string,
  data: string
): Promise<void> {
  const filenameSanitized = sanitizeFilename(filename);
  return fs.promises.writeFile(path.join(filePathBase, filenameSanitized), data, 'utf8');
}

async function saveAttachmentContentAddressed(
  url: string
): Promise<{ localPath: string; sha256: string }> {
  await mkdirIfNeeded(attachmentContentDir);
  const response = await new Promise<IncomingMessage>((res, rej) => {
    https.get(url, res).on('error', rej);
  });

  const hasher = crypto.createHash('sha256');

  const data = await new Promise<Buffer>((res, rej) => {
    const dataArray: Uint8Array[] = [];
    response
      .on('data', (chunk: Uint8Array) => {
        dataArray.push(chunk);
        hasher.update(chunk);
      })
      .on('end', () => {
        res(Buffer.concat(dataArray));
      })
      .on('error', rej);
  });

  const sha256 = hasher.digest('hex');
  const localPath = path.join(attachmentContentDir, sha256);

  await fs.promises.writeFile(localPath, data);

  return { localPath, sha256 };
}

async function saveAttachment(
  channelLogDir: string,
  att: DiscordAttachment
): Promise<SavedAttachment> {
  const attachmentAliasDir = path.join(channelLogDir, 'attachments');
  await mkdirIfNeeded(attachmentAliasDir);

  let attachmentAliasName = att.id;
  if (att.name) attachmentAliasName += ' ' + sanitizeFilename(att.name);

  const localAliasPath = path.join(attachmentAliasDir, attachmentAliasName);
  let sha256: string | undefined;
  try {
    const linkTarget = await fs.promises.readlink(localAliasPath);
    // Symlink exists.
    sha256 = path.basename(linkTarget);
  } catch (ex) {
    // Symlink doesn't exist.
  }

  if (sha256) {
    // Symlink exists.
    if (fs.existsSync(localAliasPath)) {
      // Re-hash local file to make sure.
      const hashBuffer = await new Promise<Buffer>((res, rej) => {
        const rehashPipe = fs.createReadStream(localAliasPath).pipe(crypto.createHash('sha256'));
        rehashPipe
          .on('readable', () => {
            res(rehashPipe.read());
          })
          .on('error', rej);
      });
      const actualSha256 = hashBuffer.toString('hex');
      if (sha256 !== actualSha256) {
        throw new Error('Got wrong hash for already-downloaded attachment!');
      }
    } else {
      // Symlink is broken.
      fs.promises.unlink(localAliasPath);
      sha256 = undefined;
    }
  }

  if (!sha256) {
    // Need to download.
    const saved = await saveAttachmentContentAddressed(att.url);
    sha256 = saved.sha256;

    const localPathRelativeToAlias = path.relative(attachmentAliasDir, saved.localPath);
    await fs.promises.symlink(localPathRelativeToAlias, localAliasPath, 'file');
  }

  return { originalUrl: att.url, sha256, localFilename: attachmentAliasName };
}

interface LogFileInfo {
  filename: string;
  alreadyExisted: boolean;
}

function editMsgWithFilenames(statusMsg: Message, filenames: LogFileInfo[]) {
  const fn1 = filenames[0];
  const fn2 = filenames[filenames.length - 1];

  if (filenames.length === 0) {
    return statusMsg.delete();
  }

  let extra = '\n`' + fn1.filename + '`';
  if (fn1.alreadyExisted) extra += ' (overwritten)';
  extra += '\n';
  if (filenames.length > 2) {
    extra += '...\n';
  }
  if (filenames.length > 1) {
    extra += '`' + fn2.filename + '`';
    if (fn2.alreadyExisted) extra += ' (overwritten)';
  }
  return statusMsg.edit(statusMsg.content + ':' + extra);
  /*
  // paginate results
  let reply = `... ${filenames.length} files saved!\n`;
  for (const {filename, alreadyExisted} of filenames) {
    let line = '`' + filename + '`';
    if (alreadyExisted) line += ' (overwritten)';
    line += '\n';

    const reply0 = reply;
    reply += line;
    if (reply.length > 1900) {
      reply = '...\n' + line;
      await channel.send(reply0);
    }
  }
  if (reply) {
    await channel.send(reply);
  }
  */
}

function parseargs(a: string): [string, number] {
  const args = a.split(/\s/);
  let numWeeks = 1;
  if (args.length > 2) {
    throw 'invalid args';
  }
  const start = args[0];
  if (start.length !== 10) {
    throw 'date must be YYYY-MM-DD';
  }
  if (args.length === 2) {
    numWeeks = Number(args[1]);
    if (numWeeks > 53) {
      throw '`numweeks` should be <= 53';
    }
  }
  return [start, numWeeks];
}

async function writeGuildMeta(guild: Guild) {
  await mkdirIfNeeded(logsdir);
  const serverdir = path.join(logsdir, `server=${guild.id}`);
  await mkdirIfNeeded(serverdir);
  const guildMetaFilename = path.join(serverdir, '!meta.json');

  let guildMeta: GuildMeta = { members: {}, roles: {}, textChannels: {} };
  if (fs.existsSync(guildMetaFilename)) {
    guildMeta = JSON.parse(await fs.promises.readFile(guildMetaFilename, 'utf8'));
    guildMeta.members ??= {};
    guildMeta.roles ??= {};
    guildMeta.textChannels ??= {};
  }
  for (const [id, member] of await guild.members.fetch()) {
    guildMeta.members[id] = { username: member.user.username };
  }
  for (const [id, channel] of guild.channels.cache) {
    if (channel.isText()) {
      guildMeta.textChannels[id] = { name: channel.name };
    }
  }
  for (const [id, role] of (await guild.roles.fetch()).cache) {
    guildMeta.roles[id] = { name: role.name };
  }

  await fs.promises.writeFile(guildMetaFilename, JSON.stringify(guildMeta, undefined, 1), 'utf8');
}

verbs.set('log', {
  async handler(msg, args) {
    const [start, numWeeks] = parseargs(args);

    if (msg.channel instanceof TextChannel) {
      await writeGuildMeta(msg.channel.guild);
    }

    const [statusMsg, filenames] = await impl(msg.channel, start, numWeeks);
    if (statusMsg) await editMsgWithFilenames(statusMsg, filenames);
  },
  desc:
    'startdate [numweeks] - Capture logs for `numweeks`*7 days, starting on the Monday <= `startdate`, at 10am UTC',
});

async function logall(start: string, numweeks: number) {
  // All of the guilds the client is currently handling, mapped by their IDs -
  // as long as sharding isn't being used, this will be every guild the bot is a
  // member of
  const guilds = instance.client.guilds.cache;
  // "All of the Channels that the client is currently handling, mapped by their
  // IDs - as long as sharding isn't being used, this will be every channel in
  // every guild the bot is a member of. Note that DM channels will not be
  // initially cached, and thus not be present in the Manager without their
  // explicit fetching or use."
  const channels = instance.client.channels.cache;

  for (const [, guild] of guilds) {
    await writeGuildMeta(guild);
  }

  // For every 'text' channel:
  for (const channel of channels.values()) {
    if (channel instanceof TextChannel /* || channel instanceof DMChannel */) {
      const [statusMsg, filenames] = await impl(channel, start, numweeks);
      if (statusMsg) await editMsgWithFilenames(statusMsg, filenames);
    }
  }
}

verbs.set('logall', {
  async handler(msg, args) {
    const [start, numWeeks] = parseargs(args);

    await logall(start, numWeeks);
    await msg.reply('Done!');
  },
  desc: 'startdate [numweeks] - same as /log but captures all (non-DM) channels',
});
