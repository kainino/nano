import { promises as fs } from 'fs';

import escape from 'js-string-escape';
import peg from 'pegjs';
import serialize from 'serialize-javascript';
import vm2 from 'vm2';

import { FakeMessage, PseudoID } from '../../fakeuser';
import { Instance, VerbDescriptor } from '../../plugin_manager';

export const verbs = new Map<string, VerbDescriptor>();

let instance: Instance;
export function onload(_instance: Instance) {
  instance = _instance;
}

const vms = new Map();
async function getVM(msg: FakeMessage) {
  let vm = vms.get(msg.channel.id);
  if (!vm) {
    msg.react('🥱');

    const initSource = await fs.readFile(__dirname + '/sandbox/init.js', 'utf8');
    const grammar = await fs.readFile(__dirname + '/sandbox/roll.pegjs', 'utf8');
    const parserSource = peg.generate(grammar, {
      exportVar: 'parser',
      format: 'globals',
      output: 'source',
    });

    vm = new vm2.VM({ timeout: 5000 });
    vms.set(msg.channel.id, vm);
    vm.run(initSource);
    vm.run(parserSource);
    vm.run('const parse = parser.parse;');

    const imported = await instance.storageGetItem('vm/' + msg.channel.id);
    if (imported) {
      vm.run(`Object.assign(global, ${imported});`);
    }
  }
  return vm;
}

async function exportState(msg: FakeMessage, options?: serialize.SerializeJSOptions) {
  const vm = await getVM(msg);
  const exported = vm.run('global');
  delete exported.Buffer;
  delete exported.Proxy;
  delete exported.VMError;
  delete exported.parser;
  return serialize(exported, options);
}

async function say(msg: FakeMessage, text: string) {
  text = text.toString();
  // '@' + user#0000 (37) + '(' + alias (32) + '),\n... ' + 1900 < max (2000)
  //  1               37     1           32     7           1900 <      2000
  if (text.length > 1900) {
    text = '\n... ' + text.slice(text.length - 1900, text.length);
  }
  const alias = await getAlias(msg);

  const pre = alias ? `${alias} (${msg.author}), ` : `${msg.author}, `;
  await msg.channel.send(pre + text);
}

function literalize(v: unknown) {
  if (typeof v === 'number') {
    return v.toString();
  }
  if (typeof v === 'string') {
    const asNum = Number(v);
    if (asNum === asNum) {
      return v.toString();
    }
    return `'${escape(v)}'`;
  }
  return 'undefined';
}

verbs.set('exec', {
  async handler(msg, args) {
    try {
      const result = await withLast(msg, args);
      await say(msg, `**=>** ${String(result)}`);
    } catch (e) {
      await say(msg, `**error** ${e}`);
    }
    await instance.storageSetItem('vm/' + msg.channel.id, await exportState(msg));
  },
  desc: '[javascript] - execute some JavaScript. (The `global` object is persistent.)',
});

verbs.set('export', {
  async handler(msg) {
    await say(msg, await exportState(msg, { space: 2 }));
  },
  desc: '- export the current JavaScript state',
});

const aliases = new Map();
function aliasesKeyForMessage(msg: FakeMessage) {
  return `${msg.channel.id}/${msg.author.pseudoID}`;
}
async function getAlias(msg: FakeMessage) {
  const key = aliasesKeyForMessage(msg);
  const cached = aliases.get(key);
  return cached !== undefined ? cached : await instance.storageGetItem('alias/' + key);
}
function setAlias(msg: FakeMessage, alias: string) {
  const key = aliasesKeyForMessage(msg);
  if (alias) {
    aliases.set(key, alias);
    instance.storageSetItem('alias/' + key, alias);
  } else {
    aliases.set(key, null);
    instance.storageRemoveItem('alias/' + key);
  }
}

verbs.set('rollas', {
  async handler(msg, args) {
    setAlias(msg, args.trim().slice(0, 32));
    await say(msg, 'OK');
  },
  desc: '[alias] - set a (character) name to label your rolls',
});

const lastResultForUser = new Map<PseudoID, unknown>();

async function withLast(msg: FakeMessage, code: string) {
  const vm = await getVM(msg);
  const last = literalize(lastResultForUser.get(msg.author.pseudoID));
  lastResultForUser.delete(msg.author.pseudoID);

  vm.run(`global.last = ${last};`);
  const ret = vm.run(code);
  let message, result;
  if (typeof ret === 'object' && ret.message) {
    message = ret.message;
    result = ret.result;
  } else {
    message = result = ret;
  }
  vm.run('global.last = undefined;');

  lastResultForUser.set(msg.author.pseudoID, result);
  return message;
}

verbs.set('roll', {
  async handler(msg, args) {
    // This doesn't actually have to be "secure".
    // It's just done to reduce surprises.
    args = args.replace(/"/g, "'");
    try {
      const code = `{
      const p = parse("${args}");
      let steps = '';
      let step;
      for (step of p.iterateStepPrints()) {
        steps += '\\n**=>** ' + step;
      }
      if (p.comment) { steps += ' # ' + p.comment; }
      ({ message: steps, result: step })
    }`;

      const steps = await withLast(msg, code);
      await say(msg, steps);
    } catch (e) {
      await say(msg, `**${e.constructor.name}:** ${e.message}`);
    }
  },
  desc: '[rollexpr] - roll some dice, or a whole rollexpr',
});
