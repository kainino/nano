'use strict';
const fs = require('fs');
const readline = require('readline');

const peg = require('pegjs');

global.roll1 = sides => {
  return 1 + Math.floor(Math.random() * Math.floor(sides));
};

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
  prompt: '> ',
});

(async function () {
  const grammar = await fs.promises.readFile('roll.pegjs', 'utf8');
  const parser = peg.generate(grammar);

  rl.prompt();
  rl.on('line', line => {
    if (!line.trim()) {
      rl.prompt();
      return;
    }
    try {
      const p = parser.parse(line);
      console.log('->', p.toString());
      console.log(p.analyze().toString());
      const MAX_STEPS = 8;
      let i;
      for (i = 0; i < MAX_STEPS; ++i) {
        const stepped = p.step();
        if (!stepped) {
          break;
        }
        console.log('=>', p.toString());
        console.log(p.analyze().toString());
      }
      if (p.comment) {
        console.log(' #', p.comment);
      }
      console.log(`** finished in ${i} steps **`);
      console.log();
    } catch (e) {
      console.error(`${e.constructor.name}: ${e.message}`);
      //console.log(e);
    }
    rl.prompt();
  });
})();
