'use strict';
const fs = require('fs');

const peg = require('pegjs');

const verified_tests = [
  ['(d)x(d)d(d)', '(4,)'],
  ['', '()'],

  ['1', '1'],
  ['10', '10'],

  ['12.34', '12.34'],
  ['12.', '12'],
  ['.12', '0.12'],

  ['1+2', '3'],
  ['1*2', '2'],
  ['1*2-3/4', '1.25'],

  ['-1', '-1'],
  ['-12.34', '-12.34'],
  ['1/4', '0.25'],

  ['d', '**1**'],
  ['d4', '**1**'],
  ['2d', '3'],
  ['10d20', '55'],
  ['1d', '1'],
  ['0d', '0'],
  ['d(-1)', '**1**'],

  /* It would require Math.random to return exactly 0 for this
   * to happen normally. */ ['-d', '-1'], // -(d)
  ['(-1)d', '0'], // (-1)d
  ['-(1d)', '-1'], // -(1d)
  ['-1d', '-1'], // -(1d)
  ['(-1)xd', '()'], // (-1)xd
  ['-(1xd)', '(-1,)'], // -(1xd)
  ['-1xd', '(-1,)'], // -(1xd)

  // TODO: add this syntax?
  //["ad20", "2"],  // 'max(2xd20)
  //["dd20", "1"],  // 'min(2xd20)
  // TODO: and maybe this syntax?
  //["3ad20", "3"],  // 'max(3xd20)
  //["3dd20", "1"],  // 'min(3xd20)

  ['2x4', '(4, 4)'],
  ['2xd20', '(**1**, **2**)'],
  ['2xd', '(**1**, **2**)'],
  ['2x3d', '(6, 15)'],
  ['2x3d20', '(6, 15)'],
  ['2x3d20+4', '(10, 19)'],
  ['2x3d20*4', '(24, 60)'],
  ['4+2x3d20', '(10, 19)'],
  ['4*2x3d20', '(24, 60)'],

  ['(1)', '1'],
  ['d(d20)', '**1**'],
  ['2x(3d20+4)', '(10, 19)'],
  ['2x3d(d20)', '(6, 9)'],
  ['(d1)xd', '(**2**,)'],
  ['(d4)x(d4)d(d20)', '(4,)'],
  ['(d)x(d)d(d)', '(4,)'],
  ['(d)x(d)d', '(7,)'],

  ['(0+2)x(d2)d(d2) * (1+1)x(d2)d(d2) + (2+0)x(d2)d(d2)', '(2, 6)'],
  ['2x(d20)d1', '(1, 2)'],

  ['d(2x2)', '(**1**, **2**)'],
  ['(2x2)d', '(3, 7)'],
  ['(2x2)xd', '((**1**, **2**), (**3**, **4**))'],

  ['1,1', '(1, 1)'],
  ['1,', '(1,)'],
  ['(-1,)', '(-1,)'],
  ['-(1,)', '(-1,)'],
  ['-(1,-1)', '(-1, 1)'],
  ['1 - (1,-1)', '(0, 2)'],
  ['1 + (1,-1)', '(2, 0)'],
  ['(1,-1) - 1', '(0, -2)'],
  ['(1,-1) + 1', '(2, 0)'],

  ['1 / d20', '1'],
  ["1 / 'roll1(20)", '1'],

  ["'zero", '0'],
  ["'roll1(20)", '1'],
  ["'map('max, 10x(2xd20))", '(2, 4, 6, 8, 10, 12, 14, 16, 18, 20)'],

  ["0 # o u4(#$(' 3", '0'],
  ["0 #o u4(#$(' 3", '0'],
  ["0# o u4(#$(' 3", '0'],
  ["0#o u4(#$(' 3", '0'],
];

global.zero = 0;
global.map = (f, xs) => xs.map(f);
global.max = function (x) {
  if (x instanceof Array) {
    return Math.max.apply(null, x);
  } else {
    return Math.max.apply(null, arguments);
  }
};

(async function () {
  const grammar = await fs.promises.readFile('roll.pegjs', 'utf8');
  try {
    const parser = peg.generate(grammar);

    // Now stub roll1 for verified tests.
    let stub_random = 0;
    global.roll1 = sides => {
      const int_sides = Math.floor(sides);
      if (int_sides === 0) {
        return 1;
      }
      const result = 1 + (stub_random % int_sides);
      stub_random++;
      return result;
    };

    console.log('** Verified tests **');

    for (const example of verified_tests) {
      stub_random = 0; // reset stub
      const line = example[0];
      const expected = example[1];
      const p = parser.parse(line);
      let i = 0;
      let execution_trace = `   parsed: ${p}`;
      for (const step of p.iterateStepPrints()) {
        execution_trace += `\n   step ${i++}: ${step}`;
      }
      if (expected === p.toString()) {
        console.log('[   OK   ]', line);
      } else {
        console.error('[ FAILED ]', line);
        console.error(execution_trace);
        if (p.comment) {
          console.error('           #', p.comment);
        }
        console.error(`  expected ${expected} but got ${p.toString()}`);
      }
    }
  } catch (e) {
    console.error(e);
  }
})();
