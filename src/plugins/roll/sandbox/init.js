/* eslint-disable no-unused-vars */
'use strict';
// Everything in this file should be const so it can't be accidentally
// overwritten in /exec commands.

const roll1 = sides => {
  return 1 + Math.floor(Math.random() * Math.floor(sides));
};

const zero = 0;
const reduce = (f, xs, x0) => xs.reduce(f, x0);
const map = (f, xs) => xs.map(f);
const max = function (x) {
  if (x instanceof Array) {
    return Math.max.apply(null, x);
  } else {
    return Math.max.apply(null, arguments);
  }
};
const min = function (x) {
  if (x instanceof Array) {
    return Math.min.apply(null, x);
  } else {
    return Math.min.apply(null, arguments);
  }
};
