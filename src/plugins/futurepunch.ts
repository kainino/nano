import serialize from 'serialize-javascript';

import { PseudoID, FakeMessage } from '../fakeuser';
import { Instance, VerbDescriptor } from '../plugin_manager';

export const verbs = new Map<string, VerbDescriptor>();

interface ServerState {
  currentSerial: number;
  inFlightPunches: Map<number, [PseudoID]>;
}

let instance: Instance;
let statePerServer: Map<string, ServerState>;
export const onload = async (_instance: Instance) => {
  instance = _instance;
  await load();
};

async function load() {
  const state = await instance.storageGetItem('state');
  if (state) {
    statePerServer = eval('(' + state + ')');
  } else {
    statePerServer = new Map();
  }
}

function save() {
  instance.storageSetItem('state', serialize(statePerServer));
}

function getServerState(serverID: string) {
  let state = statePerServer.get(serverID);
  if (state === undefined) {
    state = { currentSerial: 0, inFlightPunches: new Map() };
    statePerServer.set(serverID, state);
  }
  return state;
}

export const listener = async (msg: FakeMessage) => {
  if (!msg.guild) {
    return;
  }
  const state = getServerState(msg.guild.id);

  const shouldsave = false;
  const punches = state.inFlightPunches.get(state.currentSerial);
  if (punches) {
    state.inFlightPunches.delete(state.currentSerial);

    const punchee = await instance.pseudoIDToFakeUser(msg.author.pseudoID);
    for (const puncherPseudoID of punches) {
      let m;
      if (puncherPseudoID === msg.author.pseudoID) {
        m = `_STOP HITTING YOURSELF, ${punchee}. STOP HITTING YOURSELF._`;
      } else {
        const puncher = await instance.pseudoIDToFakeUser(puncherPseudoID);
        m = `_${puncher} punched ${punchee}. FROM THE PAST._`;
      }
      await msg.channel.send(m);
    }
  }

  state.currentSerial++;
  // Save a little less often just in case it impacts performance.
  // Doesn't really hurt anything because restarting Nano usually
  // misses a few messages anyways.
  if (shouldsave || state.currentSerial % 10 === 0) {
    save();
  }
};

verbs.set('futurepunch', {
  handler: async (msg, args) => {
    if (!msg.guild) {
      throw '`/futurepunch` only works in servers';
    }
    const state = getServerState(msg.guild.id);

    const futureness = args === '' ? 1 : Number(args);
    if (futureness <= 0) {
      throw '`futureness` must be a positive number';
    }

    const futureSerial = state.currentSerial + futureness;

    const punches = state.inFlightPunches.get(futureSerial);
    if (punches) {
      punches.push(msg.author.pseudoID);
    } else {
      state.inFlightPunches.set(futureSerial, [msg.author.pseudoID]);
    }
  },
  desc: ' [futureness] - punch someone. IN THE FUTURE :O',
});
