import assert from 'assert';

import { MessageReaction, Snowflake } from 'discord.js';
import cron from 'node-cron';
import serialize from 'serialize-javascript';

import { PseudoID, FakeMessage } from '../../fakeuser';
import { Instance, VerbDescriptor } from '../../plugin_manager';

import Wordlist from './words.json';

export const verbs = new Map<string, VerbDescriptor>();

type Word = string;
type UnixTimeMillis = number;

let instance: Instance;
let secretwords: Map<Word, UnixTimeMillis>;
let scoreboard: Map<Snowflake, Map<PseudoID, { points: number; trinkets: string }>>;
let history: {
  finds: {
    pseudoID: PseudoID;
    timestamp: UnixTimeMillis;
    wordCount: number;
    explanation: string;
  }[];
  adds: UnixTimeMillis[];
};
let trinketState: { active: Set<string>; idle: string[] };

export async function onload(_instance: Instance) {
  instance = _instance;

  secretwords = new Map((await instance.storageGetItem('secretwords')) as [Word, UnixTimeMillis][]);
  scoreboard = eval('(' + (await instance.storageGetItem('scoreboard')) + ')') || new Map();
  history = eval('(' + (await instance.storageGetItem('history')) + ')') || {
    finds: [],
    adds: [],
  };
  trinketState = eval('(' + (await instance.storageGetItem('trinketState')) + ')') || {
    idle: initialTrinkets,
    active: new Set(),
  };

  // Every hour
  cron.schedule(
    '0 * * * *',
    () => {
      addNewWords(1);
      addNewTrinkets(1);
    },
    { timezone: 'Etc/UTC' }
  );
}

function ageToScore(ageMS: number): [number, string] {
  const ageDays = ageMS / 1000 / 60 / 60 / 24;
  const ageD = Math.floor(ageDays);
  const ageH = Math.floor((ageDays % 1) * 24);

  return [(1 + ageDays) ** 1.25, `${ageD}ᵈ${ageH}ʰ`];
}

function randomword() {
  return Wordlist[Math.floor(Math.random() * Wordlist.length)];
}

function takeRandomTrinket() {
  const i = Math.floor(Math.random() * trinketState.idle.length);
  const t = trinketState.idle[i];
  trinketState.idle.splice(i, 1);
  return t;
}

function superscript(s: number): string {
  const superscriptNumbers = '⁰¹²³⁴⁵⁶⁷⁸⁹';
  return Array.from(s.toString())
    .map(c => superscriptNumbers[c.charCodeAt(0) - '0'.charCodeAt(0)])
    .join('');
}

function combinescores(words: FoundWord[]): [number, string, number] {
  let sum = 0;
  for (const { score } of words) {
    sum += score;
  }
  const comboBase = 4;
  const comboExp = words.length - 1;
  const combo = comboBase ** comboExp;
  const final = Math.floor(sum * combo);

  if (words.length === 1) {
    const w = words[0];
    return [final, `\`${w.word}\`, ${w.explanation} = ${final}ᵖ`, 1];
  } else {
    const explanation = words
      .map(({ word, score, explanation }) => `(\`${word}\`, ${explanation}, ${score.toFixed(2)}ᵖ)`)
      .join(' + ');
    return [
      final,
      `⌊ [ ${explanation} ] × ${comboBase}${superscript(comboExp)} combo ⌋ = ${final}ᵖ`,
      words.length,
    ];
  }
}

function scoreAndRetireTrinkets(serverID: Snowflake, pseudoID: PseudoID, str: string) {
  const possibleTrinkets = Array.from(str).filter(c => c.charCodeAt(0) > 255);
  let foundTrinkets = '';
  for (const possibleTrinket of possibleTrinkets) {
    if (trinketState.active.has(possibleTrinket)) {
      foundTrinkets += possibleTrinket;
      trinketState.active.delete(possibleTrinket);
    }
  }
  if (foundTrinkets) {
    saveScoredTrinkets(serverID, pseudoID, foundTrinkets);
    instance.storageSetItem('trinketState', serialize(trinketState));
    return foundTrinkets;
  }
  return undefined;
}

interface FoundWord {
  word: string;
  index: number;
  score: number;
  explanation: string;
}

function scoreAndRetireWords(serverID: Snowflake, pseudoID: PseudoID, str: string) {
  const now = Date.now();
  const str_clean = str.toLowerCase().replace(/[^a-z\-0-9]/g, '');

  const found: FoundWord[] = [];
  for (const [word, timestamp] of secretwords) {
    const index = str_clean.indexOf(word);
    const matches = index !== -1;
    if (matches) {
      const [score, explanation] = ageToScore(now - timestamp);
      found.push({ word, index, score, explanation });
      secretwords.delete(word);
    }
  }

  if (found.length === 0) {
    return [];
  }

  instance.storageSetItem('secretwords', Array.from(secretwords));

  found.sort((a, b) => a.index - b.index);
  const [final, explanation, wordCount] = combinescores(found);

  history.finds.push({ pseudoID, timestamp: now, wordCount, explanation });
  instance.storageSetItem('history', serialize(history));

  saveScoredPoints(serverID, pseudoID, final);
  return [found, explanation];
}

function getServerScoreboard(serverID: Snowflake) {
  let serverScoreboard = scoreboard.get(serverID);
  if (serverScoreboard === undefined) {
    serverScoreboard = new Map();
    scoreboard.set(serverID, serverScoreboard);
  }
  return serverScoreboard;
}

function getServerPlayer(serverID: Snowflake, pseudoID: PseudoID) {
  const serverScoreboard = getServerScoreboard(serverID);

  let player = serverScoreboard.get(pseudoID);
  if (player === undefined) {
    player = { points: 0, trinkets: '' };
    serverScoreboard.set(pseudoID, player);
  }
  return player;
}

function saveScoredPoints(serverID: Snowflake, pseudoID: PseudoID, points: number) {
  const player = getServerPlayer(serverID, pseudoID);
  player.points += points;
  instance.storageSetItem('scoreboard', serialize(scoreboard));
}

function saveScoredTrinkets(serverID: Snowflake, pseudoID: PseudoID, trinkets: string) {
  const player = getServerPlayer(serverID, pseudoID);
  player.trinkets += trinkets;
  instance.storageSetItem('scoreboard', serialize(scoreboard));
}

function addNewWords(count: number) {
  const newwords = [];
  for (let i = 0; i < count; ++i) {
    const word = randomword();
    newwords.push(word);
    if (!secretwords.has(word)) {
      const timestamp = Date.now();
      secretwords.set(word, timestamp);
      history.adds.push(timestamp);
    }
  }
  //instance.diagnosticChannel.send('There are new "secret" words... ' + newwords.join(', '));

  instance.storageSetItem('secretwords', Array.from(secretwords));
  instance.storageSetItem('history', serialize(history));
}

function addNewTrinkets(count: number) {
  for (let i = 0; i < count; ++i) {
    trinketState.active.add(takeRandomTrinket());
  }

  instance.storageSetItem('trinketState', serialize(trinketState));
}

const zwsp = '\u200B';
verbs.set('scores', {
  async handler(msg) {
    let m = '';
    const sortedScoreboard = Array.from(scoreboard)
      .map(([serverID, serverScoreboard]) => {
        let serverPoints = 0;
        for (const [, { points }] of serverScoreboard) {
          serverPoints += points;
        }
        return { serverID, serverScoreboard, serverPoints };
      })
      .sort((a, b) => b.serverPoints - a.serverPoints);
    for (const { serverID, serverScoreboard, serverPoints } of sortedScoreboard) {
      const server = instance.client.guilds.resolve(serverID);
      assert(server !== null);
      const serverName = server.name;
      const serverNameAbbr = serverName.split(' ', 1)[0];
      m += `\n**${serverNameAbbr}: ${serverPoints}ᵖ**`;
      if (msg.guild && msg.guild.id === serverID) {
        const sortedServerScoreboard = Array.from(serverScoreboard).sort(
          (a, b) => b[1].points - a[1].points
        );
        for (const [pseudoID, { points, trinkets }] of sortedServerScoreboard) {
          const trinketsArray = Array.from(trinkets);
          const name = (await instance.pseudoIDToFakeUser(pseudoID)).username;
          m += `\n  - ${name}: ${points}ᵖ ${trinketsArray.slice(trinketsArray.length - 1)}`;
        }
      }
    }

    const historyLengthHours = 72;
    const historyLength = historyLengthHours * 60 * 60 * 1000;
    const now = Date.now();
    let numWordsFound = 0;
    const recentFound = [];
    for (let i = history.finds.length - 1; i >= 0; --i) {
      const { pseudoID, timestamp, wordCount, explanation } = history.finds[i];
      if (now - timestamp > historyLength) {
        break;
      }
      numWordsFound += +wordCount;
      if (recentFound.length < 4) {
        const name = (await instance.pseudoIDToFakeUser(pseudoID)).username;
        recentFound.push(`  - ${name}: ${explanation}`);
      }
    }
    for (let i = history.adds.length - 1; i >= 0; --i) {
      const timestamp = history.adds[i];
      if (now - timestamp > historyLength) {
        break;
      }
    }
    m += '\n';
    m += `There are **${secretwords.size}** secret words. `;
    m += `In ${historyLengthHours}ʰ, ${numWordsFound} were found.\n`;
    m += recentFound.join('\n');
    //m += `The maximum eligible message length is ${max_message_length} characters.`

    msg.channel.send(m);
  },
  desc: '- print secret word game scores/history',
});

verbs.set('trinkets', {
  async handler(msg) {
    let m = `🕴 = ${trinketState.active.size}.`;
    const sortedScoreboard = Array.from(scoreboard)
      .map(([serverID, serverScoreboard]) => {
        let serverTrinkets = '';
        for (const [, { trinkets }] of serverScoreboard) {
          serverTrinkets += trinkets;
        }
        return { serverID, serverScoreboard, serverTrinkets };
      })
      .sort((a, b) => b.serverTrinkets.length - a.serverTrinkets.length);
    for (const { serverID, serverScoreboard, serverTrinkets } of sortedScoreboard) {
      const server = instance.client.guilds.resolve(serverID);
      assert(server !== null);
      const serverName = server.name;
      const serverNameAbbr = serverName.split(' ', 1)[0];
      m += `\n**${serverNameAbbr}:**`;
      if (msg.guild && msg.guild.id === serverID) {
        const sortedServerScoreboard = Array.from(serverScoreboard).sort(
          (a, b) => b[1].points - a[1].points
        );
        for (const [pseudoID, { trinkets }] of sortedServerScoreboard) {
          if (trinkets) {
            const name = (await instance.pseudoIDToFakeUser(pseudoID)).username;
            m += `\n  - ${name}: ${Array.from(trinkets).join(zwsp)}`;
          }
        }
      } else {
        m += ' ' + Array.from(serverTrinkets).join(zwsp);
      }
    }

    msg.channel.send(m);
  },
  desc: '- print trinket game scores',
});

const max_message_length = 240;
export async function listener(msg: FakeMessage) {
  if (!msg.guild) {
    return;
  }
  if (msg.content.length > max_message_length) {
    return;
  }

  const [found, explanation] = scoreAndRetireWords(msg.guild.id, msg.author.pseudoID, msg.content);
  if (found !== undefined) {
    await msg.reply(`㊙️💬🔍❗ ${explanation}.`);
  }

  const foundTrinkets = scoreAndRetireTrinkets(msg.guild.id, msg.author.pseudoID, msg.content);
  if (foundTrinkets) {
    await msg.reply(foundTrinkets);
  }
}

export async function reactionListener(reaction: MessageReaction, pseudoID: PseudoID) {
  if (reaction.emoji.url) return; // This is not a unicode emoji reaction
  if (!reaction.message.guild) return; // This is a direct message

  const foundTrinkets = scoreAndRetireTrinkets(
    reaction.message.guild.id,
    pseudoID,
    reaction.emoji.toString()
  );
  if (foundTrinkets) {
    const user = (await instance.pseudoIDToFakeUser(pseudoID)).toString();
    await reaction.message.channel.send(user + ', ' + Array.from(foundTrinkets).join(' '));
  }
}

// One trinket is one codepoint.
// These are the 1367 codepoints officially called "emoji" in Unicode 13.0.
const initialTrinkets = Array.from(
  `
’“”‼⁉⃣™ℹ↔↕↖↗↘↙↩↪⌚⌛⌨⏏⏩⏪⏫⏬⏭⏮⏯⏰⏱⏲⏳⏸⏹⏺Ⓜ▪▫▶◀◻◼◽◾☀☁☂☃☄☎☑☔☕☘☝☠☢☣☦☪☮☯☸☹☺♀♂♈♉♊♋♌♍♎♏♐♑♒♓♟♠♣♥♦♨♻♾♿⚒⚓⚔⚕⚖⚗⚙⚛⚜⚠⚡⚧⚪⚫⚰⚱⚽⚾⛄⛅⛈⛎⛏⛑⛓⛔⛩⛪⛰⛱⛲⛳⛴⛵⛷⛸⛹⛺⛽✂✅✈✉✊✋✌✍✏✒✔✖✝✡✨✳✴❄❇❌❎❓❔❕❗❣❤➕➖➗➡➰➿⤴⤵⬅⬆⬇⬛⬜⭐⭕〰〽㊗㊙🀄🃏🅰🅱🅾🅿🆎🆑🆒🆓🆔🆕🆖🆗🆘🆙🆚🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿🈁🈂🈚🈯🈲🈳🈴🈵🈶🈷🈸🈹🈺🉐🉑🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌍🌎🌏🌐🌑🌒🌓🌔🌕🌖🌗🌘🌙🌚🌛🌜🌝🌞🌟🌠🌡🌤🌥🌦🌧🌨🌩🌪🌫🌬🌭🌮🌯🌰🌱🌲🌳🌴🌵🌶🌷🌸🌹🌺🌻🌼🌽🌾🌿🍀🍁🍂🍃🍄🍅🍆🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓🍔🍕🍖🍗🍘🍙🍚🍛🍜🍝🍞🍟🍠🍡🍢🍣🍤🍥🍦🍧🍨🍩🍪🍫🍬🍭🍮🍯🍰🍱🍲🍳🍴🍵🍶🍷🍸🍹🍺🍻🍼🍽🍾🍿🎀🎁🎂🎃🎄🎅🎆🎇🎈🎉🎊🎋🎌🎍🎎🎏🎐🎑🎒🎓🎖🎗🎙🎚🎛🎞🎟🎠🎡🎢🎣🎤🎥🎦🎧🎨🎩🎪🎫🎬🎭🎮🎯🎰🎱🎲🎳🎴🎵🎶🎷🎸🎹🎺🎻🎼🎽🎾🎿🏀🏁🏂🏃🏄🏅🏆🏇🏈🏉🏊🏋🏌🏍🏎🏏🏐🏑🏒🏓🏔🏕🏖🏗🏘🏙🏚🏛🏜🏝🏞🏟🏠🏡🏢🏣🏤🏥🏦🏧🏨🏩🏪🏫🏬🏭🏮🏯🏰🏳🏴🏵🏷🏸🏹🏺🏻🏼🏽🏾🏿🐀🐁🐂🐃🐄🐅🐆🐇🐈🐉🐊🐋🐌🐍🐎🐏🐐🐑🐒🐓🐔🐕🐖🐗🐘🐙🐚🐛🐜🐝🐞🐟🐠🐡🐢🐣🐤🐥🐦🐧🐨🐩🐪🐫🐬🐭🐮🐯🐰🐱🐲🐳🐴🐵🐶🐷🐸🐹🐺🐻🐼🐽🐾🐿👀👁👂👃👄👅👆👇👈👉👊👋👌👍👎👏👐👑👒👓👔👕👖👗👘👙👚👛👜👝👞👟👠👡👢👣👤👥👦👧👨👩👪👫👬👭👮👯👰👱👲👳👴👵👶👷👸👹👺👻👼👽👾👿💀💁💂💃💄💅💆💇💈💉💊💋💌💍💎💏💐💑💒💓💔💕💖💗💘💙💚💛💜💝💞💟💠💡💢💣💤💥💦💧💨💩💪💫💬💭💮💯💰💱💲💳💴💵💶💷💸💹💺💻💼💽💾💿📀📁📂📃📄📅📆📇📈📉📊📋📌📍📎📏📐📑📒📓📔📕📖📗📘📙📚📛📜📝📞📟📠📡📢📣📤📥📦📧📨📩📪📫📬📭📮📯📰📱📲📳📴📵📶📷📸📹📺📻📼📽📿🔀🔁🔂🔃🔄🔅🔆🔇🔈🔉🔊🔋🔌🔍🔎🔏🔐🔑🔒🔓🔔🔕🔖🔗🔘🔙🔚🔛🔜🔝🔞🔟🔠🔡🔢🔣🔤🔥🔦🔧🔨🔩🔪🔫🔬🔭🔮🔯🔰🔱🔲🔳🔴🔵🔶🔷🔸🔹🔺🔻🔼🔽🕉🕊🕋🕌🕍🕎🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛🕜🕝🕞🕟🕠🕡🕢🕣🕤🕥🕦🕧🕯🕰🕳🕴🕵🕶🕷🕸🕹🕺🖇🖊🖋🖌🖍🖐🖕🖖🖤🖥🖨🖱🖲🖼🗂🗃🗄🗑🗒🗓🗜🗝🗞🗡🗣🗨🗯🗳🗺🗻🗼🗽🗾🗿😀😁😂😃😄😅😆😇😈😉😊😋😌😍😎😏😐😑😒😓😔😕😖😗😘😙😚😛😜😝😞😟😠😡😢😣😤😥😦😧😨😩😪😫😬😭😮😯😰😱😲😳😴😵😶😷😸😹😺😻😼😽😾😿🙀🙁🙂🙃🙄🙅🙆🙇🙈🙉🙊🙋🙌🙍🙎🙏🚀🚁🚂🚃🚄🚅🚆🚇🚈🚉🚊🚋🚌🚍🚎🚏🚐🚑🚒🚓🚔🚕🚖🚗🚘🚙🚚🚛🚜🚝🚞🚟🚠🚡🚢🚣🚤🚥🚦🚧🚨🚩🚪🚫🚬🚭🚮🚯🚰🚱🚲🚳🚴🚵🚶🚷🚸🚹🚺🚻🚼🚽🚾🚿🛀🛁🛂🛃🛄🛅🛋🛌🛍🛎🛏🛐🛑🛒🛕🛖🛗🛠🛡🛢🛣🛤🛥🛩🛫🛬🛰🛳🛴🛵🛶🛷🛸🛹🛺🛻🛼🟠🟡🟢🟣🟤🟥🟦🟧🟨🟩🟪🟫🤌🤍🤎🤏🤐🤑🤒🤓🤔🤕🤖🤗🤘🤙🤚🤛🤜🤝🤞🤟🤠🤡🤢🤣🤤🤥🤦🤧🤨🤩🤪🤫🤬🤭🤮🤯🤰🤱🤲🤳🤴🤵🤶🤷🤸🤹🤺🤼🤽🤾🤿🥀🥁🥂🥃🥄🥅🥇🥈🥉🥊🥋🥌🥍🥎🥏🥐🥑🥒🥓🥔🥕🥖🥗🥘🥙🥚🥛🥜🥝🥞🥟🥠🥡🥢🥣🥤🥥🥦🥧🥨🥩🥪🥫🥬🥭🥮🥯🥰🥱🥲🥳🥴🥵🥶🥷🥸🥺🥻🥼🥽🥾🥿🦀🦁🦂🦃🦄🦅🦆🦇🦈🦉🦊🦋🦌🦍🦎🦏🦐🦑🦒🦓🦔🦕🦖🦗🦘🦙🦚🦛🦜🦝🦞🦟🦠🦡🦢🦣🦤🦥🦦🦧🦨🦩🦪🦫🦬🦭🦮🦯🦰🦱🦲🦳🦴🦵🦶🦷🦸🦹🦺🦻🦼🦽🦾🦿🧀🧁🧂🧃🧄🧅🧆🧇🧈🧉🧊🧋🧍🧎🧏🧐🧑🧒🧓🧔🧕🧖🧗🧘🧙🧚🧛🧜🧝🧞🧟🧠🧡🧢🧣🧤🧥🧦🧧🧨🧩🧪🧫🧬🧭🧮🧯🧰🧱🧲🧳🧴🧵🧶🧷🧸🧹🧺🧻🧼🧽🧾🧿🩰🩱🩲🩳🩴🩸🩹🩺🪀🪁🪂🪃🪄🪅🪆🪐🪑🪒🪓🪔🪕🪖🪗🪘🪙🪚🪛🪜🪝🪞🪟🪠🪡🪢🪣🪤🪥🪦🪧🪨🪰🪱🪲🪳🪴🪵🪶🫀🫁🫂🫐🫑🫒🫓🫔🫕🫖󠁢󠁣󠁥󠁧󠁬󠁮󠁳󠁴󠁷󠁿
`.trim()
);
