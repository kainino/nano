import * as Discord from 'discord.js';

import { FakeMessage } from './fakeuser';

const bridgeMessagePattern = /^\*\*(.*?)\*\*: (.*)/;
export function makeFakeMessage(msg: Discord.Message): FakeMessage | undefined {
  if (msg.channel instanceof Discord.NewsChannel) return;

  let authorMentionName: string;
  let authorNonMentionName;
  let authorPseudoID;
  let contentWithoutAuthorName;
  if (msg.author.bot) {
    // msg.author.bot is the easiest way to detect a bridge.
    // Test if it matches the pattern: '**author**: message'
    const match = bridgeMessagePattern.exec(msg.content);

    // Not interested in other bot messages.
    if (!match) return;

    authorMentionName = match[1];
    authorNonMentionName = authorMentionName;
    authorPseudoID = authorMentionName;
    contentWithoutAuthorName = match[2];
  } else {
    authorMentionName = msg.author.toString();
    authorNonMentionName = msg.author.username;
    authorPseudoID = msg.author.id;
    contentWithoutAuthorName = msg.content;
  }

  return {
    content: contentWithoutAuthorName,
    guild: msg.guild,
    channel: msg.channel,
    author: {
      pseudoID: authorPseudoID,
      username: authorNonMentionName,
      toString() {
        return authorMentionName;
      },
    },
    reply(text: string) {
      if (msg.channel.type !== 'dm') {
        text = authorMentionName + ', ' + text;
      }
      return msg.channel.send(text);
    },
    react: msg.react.bind(msg),
  };
}
