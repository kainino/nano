import assert from 'assert';
import { promises as fs } from 'fs';

import * as Discord from 'discord.js';

import { makeFakeMessage } from './fakemessage';
import { PluginManager } from './plugin_manager';

process.on('unhandledRejection', ex => {
  throw ex;
});
(async function () {
  // Initialize Discord connection

  const config = JSON.parse(await fs.readFile('_localdata/config.json', 'utf8'));

  const intents = new Discord.Intents([Discord.Intents.NON_PRIVILEGED, 'GUILD_MEMBERS']);
  const client = new Discord.Client({ ws: { intents } });
  await client.login(config.token);
  assert(client.user !== null, 'Failed to log in');
  console.log(`Logged in as: ${client.user.tag}
  https://discordapp.com/channels/@me/${client.user.id}`);

  const diagnosticChannel = await client.channels.fetch(config.diagnosticChannel);
  assert(diagnosticChannel instanceof Discord.TextChannel, 'diagnosticChannel must be TextChannel');
  console.log(`Diagnostic channel is: #${diagnosticChannel.name}
  https://discordapp.com/channels/${diagnosticChannel.guild.id}/${diagnosticChannel.id}`);

  // Initialize plugins

  const manager = new PluginManager();
  manager.registerPlugin('logger', await import('./plugins/logger'));
  manager.registerPlugin('roll', await import('./plugins/roll'));
  //manager.registerPlugin('secret_word', await import('./plugins/secret_word'));
  manager.registerPlugin('futurepunch', await import('./plugins/futurepunch'));
  manager.registerPlugin('autoreply', await import('./plugins/autoreply'));

  await manager.onload(client, diagnosticChannel);

  client.on('message', async msg => {
    if (client.user === msg.author) return;

    const fakemsg = makeFakeMessage(msg);
    if (fakemsg === undefined) return;
    await manager.onmessage(fakemsg);
  });

  client.on('messageReactionAdd', async (reaction, user) => {
    await manager.onreaction(reaction, user.id);
  });
})();
