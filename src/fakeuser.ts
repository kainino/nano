import * as Discord from 'discord.js';

export type PseudoID = string;

export interface RealOrFakeUser {
  readonly username: string;
  toString(): string;
}

export interface FakeUser extends RealOrFakeUser {
  readonly pseudoID: PseudoID;
}

export interface FakeMessage {
  readonly author: FakeUser;
  readonly content: string;
  readonly guild: Discord.Guild | null;
  readonly channel: Discord.TextChannel | Discord.DMChannel;
  reply(msg: string): Promise<Discord.Message>;
  react(emoji: Discord.EmojiResolvable): Promise<Discord.MessageReaction>;
}

const pseudoIDIsDiscordId = /\d{18}/;
export async function pseudoIDToFakeUser(
  client: Discord.Client,
  pseudoID: PseudoID
): Promise<RealOrFakeUser> {
  if (pseudoIDIsDiscordId.test(pseudoID)) {
    return client.users.fetch(pseudoID);
  } else {
    const abbreviatedName = pseudoID.replace(/( [^ ])[^ ]*/g, (match, lastInitial) => lastInitial);
    return {
      username: abbreviatedName,
      toString() {
        return pseudoID;
      },
    };
  }
}
