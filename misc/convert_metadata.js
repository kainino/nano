'use strict';
const fs = require('fs');
const path = require('path');

for (const dirname of fs.readdirSync('.')) {
  if (!dirname.startsWith('ch=')) continue;
  if (!fs.statSync(dirname).isDirectory()) throw new Error('expected */');

  const targetDir = path.join('newformat', dirname);
  fs.mkdirSync(targetDir, { recursive: true });

  const nameToDate = new Map();
  let mostRecentName, mostRecentNameTimestamp;
  let mostRecentLogsFilepath, mostRecentFilenameNoName;
  for (const logsFilename of fs.readdirSync(dirname)) {
    if (!logsFilename.endsWith('.json')) throw new Error('expected */*.json: ' + logsFilename);
    if (!/^20/.test(logsFilename)) continue;

    const filenameLastHash = logsFilename.lastIndexOf('#');
    const channelName = logsFilename.substring(filenameLastHash, logsFilename.length - 5);

    const logsFilepath = path.join(dirname, logsFilename);
    const contents = fs.readFileSync(logsFilepath, 'utf-8');
    const lastCloseBrace = contents.lastIndexOf('}');
    const lastNewline = contents.lastIndexOf('\n', lastCloseBrace);
    const lastMessageJSON = contents.substring(lastNewline + 1, lastCloseBrace + 1);
    const lastMessage = JSON.parse(lastMessageJSON);
    const lastTimestamp = new Date(lastMessage.timestamp);

    const prevLastTimestamp = nameToDate.get(channelName);
    if (prevLastTimestamp === undefined || lastTimestamp > prevLastTimestamp) {
      nameToDate.set(channelName, lastTimestamp);
    }
    if (mostRecentNameTimestamp === undefined || lastTimestamp > mostRecentNameTimestamp) {
      mostRecentNameTimestamp = lastTimestamp;
      mostRecentName = channelName;
      mostRecentLogsFilepath = logsFilepath;
      mostRecentFilenameNoName = logsFilename.substring(0, filenameLastHash - 1) + '.json';
    }

    const newLogsFilepath = path.join(targetDir, mostRecentFilenameNoName);
    fs.copyFileSync(mostRecentLogsFilepath, newLogsFilepath);
    fs.utimesSync(newLogsFilepath, mostRecentNameTimestamp, mostRecentNameTimestamp);
  }
  if (!mostRecentNameTimestamp) throw new Error('expected a file in dir');

  for (const [name, date] of nameToDate) {
    const akaFilepath = path.join(targetDir, `!aka ${name}.json`);
    const aka = { thisNameLastSeenOn: date };
    fs.writeFileSync(akaFilepath, JSON.stringify(aka));
    fs.utimesSync(akaFilepath, date, date);
  }

  const metadataFilepath = path.join(targetDir, '!meta.json');
  const metadata = { mostRecentlyKnownName: mostRecentName };
  fs.writeFileSync(metadataFilepath, JSON.stringify(metadata));
  fs.utimesSync(metadataFilepath, mostRecentNameTimestamp, mostRecentNameTimestamp);
}
