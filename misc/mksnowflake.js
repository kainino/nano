/* eslint-disable node/no-unpublished-require */
'use strict';
const Long = require('long');

const args = process.argv.slice(2);
if (args.length === 4) {
  const [timestamp, workerID, processID, increment] = args;
  console.error({ timestamp, workerID, processID, increment });

  const snowflake =
    pad(parseInt(timestamp, 10).toString(2), 42) +
    pad(parseInt(workerID, 10).toString(2), 5) +
    pad(parseInt(processID, 10).toString(2), 5) +
    pad(parseInt(increment, 10).toString(2), 12);
  console.assert(snowflake.length === 64);
  console.log(Long.fromString(snowflake, 2).toString());
} else if (args.length === 1) {
  const snowflake = pad(Long.fromString(args[0]).toString(2), 64);
  console.error(snowflake);
  console.log({
    timestamp: parseInt(snowflake.substring(0, 42), 2),
    workerID: parseInt(snowflake.substring(42, 47), 2),
    processID: parseInt(snowflake.substring(47, 52), 2),
    increment: parseInt(snowflake.substring(52, 64), 2),
    binary: snowflake,
  });
} else {
  throw 'bad args';
}

function pad(v, n, c = '0') {
  console.assert(String(v).length <= n);
  return String(v).length === n ? String(v) : (String(c).repeat(n) + v).slice(-n);
}
